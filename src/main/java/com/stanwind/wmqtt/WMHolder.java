package com.stanwind.wmqtt;

import com.stanwind.wmqtt.auth.AuthSettingFactory;
import com.stanwind.wmqtt.auth.domain.MQTTClientConnData;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;

import java.util.List;

/**
 * Windmq holder
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2021-02-03 12:00 PM
 **/
public class WMHolder {
    public final MqttPahoMessageDrivenChannelAdapter adapter;
    public final AuthSettingFactory factory;

    public WMHolder(MqttPahoMessageDrivenChannelAdapter adapter, AuthSettingFactory factory) {
        this.adapter = adapter;
        this.factory = factory;
    }

    public MqttPahoMessageDrivenChannelAdapter getAdapter() {
        return adapter;
    }

    public AuthSettingFactory getFactory() {
        return factory;
    }

    /**
     * 订阅topic
     * @param topic
     */
    public void addTopic(String... topic) {
        adapter.addTopic(topic);
    }

    /**
     * 订阅topic
     * @param topic
     * @param qos
     */
    public void addTopic(String topic, int qos) {
        adapter.addTopic(topic, qos);
    }

    /**
     * 订阅topic
     * @param topic
     * @param qos
     */
    public void addTopics(String[] topic, int[] qos) {
        adapter.addTopics(topic, qos);
    }

    /**
     * 移除订阅topic
     * @param topic
     */
    public void removeTopic(String... topic) {
        adapter.removeTopic(topic);
    }

    /**
     * 客户端获取链接
     */
    public MQTTClientConnData getClientConnData(List<String> readTopics, List<String> writeTopics) throws Exception {
        return factory.getClientConnData(readTopics, writeTopics);
    }

    public boolean online(String clientId) {
        return factory.online(clientId);
    }
}
