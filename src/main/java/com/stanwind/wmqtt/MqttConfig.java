package com.stanwind.wmqtt;

import com.stanwind.wmqtt.auth.AuthSettingFactory;
import com.stanwind.wmqtt.auth.aliyun.AliSignSettingFactory;
import com.stanwind.wmqtt.auth.def.DefaultSettingFactory;
import com.stanwind.wmqtt.auth.domain.MQTTBasicAuthInfo;
import com.stanwind.wmqtt.auth.emqx.EmqxSettingFactory;
import com.stanwind.wmqtt.message.BytesMessageConverter;
import com.stanwind.wmqtt.security.DefMessageEncrypt;
import com.stanwind.wmqtt.security.IMsgEncrypt;
import com.stanwind.wmqtt.security.TableMsgEncrypt;
import com.stanwind.wmqtt.utils.PlatformUtils;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mapping.BytesMessageMapper;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.MqttMessageConverter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.util.StringUtils;

import static com.stanwind.wmqtt.beans.Constant.CHANNEL_NAME_OUT;

/**
 * MqttConfig
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2020-11-10 19:18
 **/
@Configuration
@EnableIntegration
@IntegrationComponentScan(basePackages = "com.stanwind.wmqtt")
public class MqttConfig {

    private static final Logger log = LoggerFactory.getLogger(MqttConfig.class);

    /**
     * 实例标志 MAC_PORT
     */
    private static String L_INSTANCE_ID = "";

    @Value("${mqtt.server-uris}")
    private String[] serverURIs;

    /**
     * 缺省外网地址则返回统一地址 否则返回该
     */
    @Value("${mqtt.pub-server-uris:}")
    private String[] pubServerURIs;

    /**
     * wss
     */
    @Value("${mqtt.websocket-uri:}")
    private String[] websocketUri;

    @Value("${mqtt.aliyun:false}")
    @Deprecated
    private Boolean aliyun;

    @Value("${mqtt.ons-endpoint:onsmqtt.cn-shenzhen.aliyuncs.com}")
    private String onsEndpoint;

    @Value("${mqtt.auth:}")
    private String auth;

    @Value("${mqtt.ali-instance:}")
    private String aliInstance;

    @Value("${mqtt.access-key:}")
    private String accessKey;

    @Value("${mqtt.secret-key:}")
    private String secretKey;

    @Value("${mqtt.jwt-key:}")
    private String jwtKey;

    @Value("${mqtt.username:}")
    private String username;

    @Value("${mqtt.password:}")
    private String password;

    @Value("${mqtt.keep-alive-interval:}")
    private int keepAliveInterval;

    @Value("${mqtt.sub-topics:}")
    private String[] subTopics;

    @Value("${mqtt.message-mapper:com.stanwind.wmqtt.message.MqttJsonMessageMapper}")
    private Class<? extends BytesMessageMapper> mapperClass;

    @Value("${mqtt.client-id-prefix:}")
    private String clientIdPrefix;

    @Value("${mqtt.expire:43200000}")
    private Long expire;

    @Value("${mqtt.acl.read:}")
    private String aclRead;

    @Value("${mqtt.acl.write:}")
    private String aclWrite;

    @Value("${mqtt.enc-table:}")
    private String encTable;

    @Value("${mqtt.enc-count:0}")
    private Integer encCount;

    public String[] getServerURIs() {
        return serverURIs;
    }

    public String[] getPubServerURIs() {
        return pubServerURIs;
    }

    public String[] getWebsocketUri() {
        return websocketUri;
    }

    @Deprecated
    public Boolean getAliyun() {
        return aliyun;
    }

    public String getOnsEndpoint() {
        return onsEndpoint;
    }

    public String getAuth() {
        return auth;
    }

    public String getAliInstance() {
        return aliInstance;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getJwtKey() {
        return jwtKey;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getKeepAliveInterval() {
        return keepAliveInterval;
    }

    public String[] getSubTopics() {
        return subTopics;
    }

    public String getClientIdPrefix() {
        return clientIdPrefix;
    }

    public String getAclRead() {
        return aclRead;
    }

    public String getAclWrite() {
        return aclWrite;
    }

    public String getEncTable() {
        return encTable;
    }

    public Integer getEncCount() {
        return encCount;
    }

    public Long getExpire() {
        return expire;
    }

    /**
     * 配置鉴权基础数据包装bean
     * @return bean
     */
    @Bean
    @ConditionalOnMissingBean(MQTTBasicAuthInfo.class)
    public MQTTBasicAuthInfo authBean() {
        return new MQTTBasicAuthInfo().setInstanceId(aliInstance)
                .setPassword(password).setUsername(username)
                .setAccessKey(accessKey).setSecretKey(secretKey)
                .setJwtKey(jwtKey);
    }

    /**
     * 连接数据生成工厂
     * @return bean
     */
    @Bean
    @ConditionalOnMissingBean(AuthSettingFactory.class)
    public AuthSettingFactory settingFactory() {
        if (StringUtils.isEmpty(auth)) {
            //旧版兼容是否阿里云
            if (aliyun) {
                return new AliSignSettingFactory();
            } else {
                return new DefaultSettingFactory();
            }
        }

        if ("aliyun".equals(auth)) {
            return new AliSignSettingFactory();
        }

        if ("emqx".equals(auth)) {
            return new EmqxSettingFactory();
        }

        return new DefaultSettingFactory();
    }

    /**
     * 固定为查表加密
     * @return 加密bean
     */
    @Bean
    @ConditionalOnMissingBean(IMsgEncrypt.class)
    public IMsgEncrypt messageEncrypt() {
        if (encCount > 0 && !StringUtils.isEmpty(encTable)) {
            log.info("MQTT配置数据表加密");
            return new TableMsgEncrypt();
        } else {
            log.info("MQTT配置数据不加密");
            return new DefMessageEncrypt();
        }
    }

    /**
     * message包装加密配置
     * @param modelPackages
     * @param encrypt
     * @return bean
     * @throws NoSuchMethodException
     */
    @Bean
    public MqttMessageConverter bytesMessageConverter(@Value("${mqtt.model-packages:com.stanwind.wmqtt.message}") String modelPackages
            , IMsgEncrypt encrypt)
            throws NoSuchMethodException {
        BytesMessageMapper bytesMessageMapper =
                BeanUtils.instantiateClass(mapperClass.getConstructor(String.class, IMsgEncrypt.class)
                        , modelPackages, encrypt);
        return new BytesMessageConverter(bytesMessageMapper);
    }

    /**
     * 实例名规则
     * @return 当前运行实例名
     */
    public String getInstanceId() {
        if (StringUtils.isEmpty(L_INSTANCE_ID)) {
            L_INSTANCE_ID = PlatformUtils.getMACAddress() + "_" + PlatformUtils.JVMPid();
        }

        return L_INSTANCE_ID;
    }

    /**
     * mqtt发送bean
     * @param mqttMessageConverter
     * @param settingFactory
     * @return 消息处理器
     * @throws Exception
     */
    @Bean
    @ServiceActivator(inputChannel = CHANNEL_NAME_OUT)
    public MessageHandler mqttOutbound(MqttMessageConverter mqttMessageConverter, AuthSettingFactory settingFactory)
            throws Exception {
        String clientId = clientIdPrefix + "_outbound_" + getInstanceId();

        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        MqttConnectOptions options = settingFactory
                .getServerConnData(serverURIs, keepAliveInterval, authBean().setClientId(clientId)).getConnectOptions();
        factory.setConnectionOptions(options);
        log.info("当前MQTT配置项: {}", options);

        MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler(clientId, factory);
        messageHandler.setConverter(mqttMessageConverter);
        messageHandler.setCompletionTimeout(5000);
        messageHandler.setAsync(true);
        log.info("outbound: {}", clientId);

        return messageHandler;
    }

    /**
     * mqtt接收bean
     * @param mqttMessageConverter
     * @param settingFactory
     * @return 消息发送客户端
     * @throws Exception
     */
    @Bean
    @Primary
    public MessageProducer mqttInbound(MqttMessageConverter mqttMessageConverter, AuthSettingFactory settingFactory)
            throws Exception {
        String clientId = clientIdPrefix + "_inbound_" + getInstanceId();

        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        MqttConnectOptions options = settingFactory
                .getServerConnData(serverURIs, keepAliveInterval, authBean().setClientId(clientId)).getConnectOptions();
        factory.setConnectionOptions(options);

        MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter(clientId, factory,
                subTopics);
        adapter.setConverter(mqttMessageConverter);
        adapter.setOutputChannel(mqttInboundChannel());
        adapter.setCompletionTimeout(5000);
        adapter.setQos(1);
        log.info("inbound: {}", clientId);

        return adapter;
    }

    @Bean
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel mqttInboundChannel() {
        return new DirectChannel();
    }

    /**
     * 发送客户端holder
     * producer暴露控制器
     * @param producer
     * @return holder
     */
    @Bean
    public WMHolder getWMHolder(MessageProducer producer, AuthSettingFactory factory) {
        return new WMHolder((MqttPahoMessageDrivenChannelAdapter) producer, factory);
    }
}
