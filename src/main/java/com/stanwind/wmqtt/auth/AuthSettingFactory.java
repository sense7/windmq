package com.stanwind.wmqtt.auth;

import com.stanwind.wmqtt.auth.domain.MQTTBasicAuthInfo;
import com.stanwind.wmqtt.auth.domain.MQTTClientConnData;
import com.stanwind.wmqtt.auth.domain.MQTTServerConnData;

import java.util.List;

/**
 * AuthSettingFactory 鉴权数据生成
 *
 * @author : stan
 * @version : 1.0
 * @date :  2022/4/10 1:06 PM
 **/
public interface AuthSettingFactory {

    /**
     * 获取登录信息
     * @param readTopics 可读topics
     * @param writeTopics 可写topics
     * @return
     * @throws Exception
     */
    MQTTClientConnData getClientConnData(List<String> readTopics, List<String> writeTopics) throws Exception;

    /**
     * 置入基础数据生成统一连接数据
     * @param serverURIs
     * @param keepAliveInterval
     * @param authBean
     * @return
     * @throws Exception
     */
    MQTTServerConnData getServerConnData(String[] serverURIs, Integer keepAliveInterval, MQTTBasicAuthInfo authBean) throws Exception;

    /**
     * 是否在线
     * @param clientId
     * @return
     */
    default boolean online(String clientId) {
        return false;
    }
}
