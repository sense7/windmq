package com.stanwind.wmqtt.auth.emqx;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.stanwind.wmqtt.MqttConfig;
import com.stanwind.wmqtt.auth.AuthSettingFactory;
import com.stanwind.wmqtt.auth.domain.MQTTBasicAuthInfo;
import com.stanwind.wmqtt.auth.domain.MQTTClientConnData;
import com.stanwind.wmqtt.auth.domain.MQTTServerConnData;
import com.stanwind.wmqtt.utils.HttpExecutor;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.eclipse.paho.client.mqttv3.MqttConnectOptions.MQTT_VERSION_3_1_1;

/**
 * DefaultSettingFactory 默认不处理
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2020-11-11 15:44
 **/

public class EmqxSettingFactory implements AuthSettingFactory {
    private static final Logger log = LoggerFactory.getLogger(EmqxSettingFactory.class);

    @Autowired
    private MQTTBasicAuthInfo authBean;

    @Resource
    private HttpExecutor httpExecutor;

    @Resource
    private MqttConfig mqttConfig;
    public static Map<String, String> HEADER = Collections.emptyMap();

    @PostConstruct
    public void init() {
        String key = "Basic " + Base64Utils.encodeToString(
                (mqttConfig.getAccessKey() + ":" + mqttConfig.getSecretKey()).getBytes(StandardCharsets.UTF_8));
        HEADER = new HashMap<>();
        HEADER.put("Authorization", key);
        log.debug("Authorization: {}", key);
    }

    @Override
    public MQTTClientConnData getClientConnData(List<String> readTopics, List<String> writeTopics) throws Exception {
        Long expire = System.currentTimeMillis() + mqttConfig.getExpire();
        Map<String/*action*/, List<String>/*topics*/> aclMap = new HashMap<>();
        if (readTopics != null && !readTopics.isEmpty()) {
            aclMap.put("sub", readTopics);
        }

        if (writeTopics != null && !writeTopics.isEmpty()) {
            aclMap.put("pub", writeTopics);
        }

        String username = "Token|Auth";
        Algorithm algorithm = Algorithm.HMAC256(authBean.getJwtKey());
        String token = JWT.create()
                .withIssuer("mqtt")  //发布者
                .withExpiresAt(new Date(expire))  //设置过期时间，例如1小时后
                .withClaim("username", username)  //可以设置自定义的payload
                .withClaim("acl", aclMap)
                .sign(algorithm);

        MQTTClientConnData data = new MQTTClientConnData().setUsername(username).setPassword(token).setExpire(expire);
        log.info("emqx jwt 连接数据: {} \r\nRead: {}\r\nWrite: {}", data.toString(), readTopics, writeTopics);

        return data;
    }

    @Override
    public MQTTServerConnData getServerConnData(String[] serverURIs, Integer keepAliveInterval, MQTTBasicAuthInfo authBean) {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setServerURIs(serverURIs);
        options.setUserName(authBean.getUsername());
        options.setPassword(authBean.getPassword().toCharArray());
        options.setKeepAliveInterval(keepAliveInterval);
        options.setCleanSession(false);
        options.setKeepAliveInterval(keepAliveInterval);
        options.setAutomaticReconnect(true);
        options.setMqttVersion(MQTT_VERSION_3_1_1);
        options.setConnectionTimeout(5000);

        return new MQTTServerConnData().setConnectOptions(options);
    }

    @Override
    public boolean online(String clientId) {
        if (StringUtils.isEmpty(clientId)) {
            log.error("查询在线的clientId为空");
            return false;
        }
        String url = "http://" + mqttConfig.getOnsEndpoint() + "/api/v5/clients/" + clientId;
        String res = httpExecutor.doGet(url, HEADER, null);
        log.trace("查询在线响应: {}", res);
        Map<String, String> resp = JSONObject.parseObject(res, new TypeReference<Map<String, String>>() {
        });
        if (resp != null && resp.containsKey("connected")) {
            return resp.getOrDefault("connected", "false").equalsIgnoreCase("true");
        }

        return false;
    }
}
