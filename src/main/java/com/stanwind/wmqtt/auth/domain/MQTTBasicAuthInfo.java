package com.stanwind.wmqtt.auth.domain;

import java.io.Serializable;

/**
 * MQTTBasicAuthInfo 鉴权基础数据包装
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2020-11-11 15:42
 **/
public class MQTTBasicAuthInfo implements Serializable {
    public static final long serialVersionUID = 1L;

    private String instanceId;
    private String accessKey;
    private String secretKey;
    /**
     * emqx用
     */
    private String jwtKey;

    private String clientId;
    private String username;
    private String password;

    public String getInstanceId() {
        return instanceId;
    }

    public MQTTBasicAuthInfo setInstanceId(String instanceId) {
        this.instanceId = instanceId;
        return this;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public MQTTBasicAuthInfo setAccessKey(String accessKey) {
        this.accessKey = accessKey;
        return this;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public MQTTBasicAuthInfo setSecretKey(String secretKey) {
        this.secretKey = secretKey;
        return this;
    }

    public MQTTBasicAuthInfo setJwtKey(String jwtKey) {
        this.jwtKey = jwtKey;
        return this;
    }

    public String getJwtKey() {
        return jwtKey;
    }

    public String getClientId() {
        return clientId;
    }

    public MQTTBasicAuthInfo setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public MQTTBasicAuthInfo setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public MQTTBasicAuthInfo setPassword(String password) {
        this.password = password;
        return this;
    }
}
