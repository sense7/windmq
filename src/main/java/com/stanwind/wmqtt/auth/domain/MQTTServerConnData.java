package com.stanwind.wmqtt.auth.domain;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;

import java.io.Serializable;

/**
 * MQTTServerConnData 当前服务端mqtt连接数据
 * 当前只包装 MqttConnectOptions
 * 方便日后扩展
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2020-11-20 14:40
 **/
public class MQTTServerConnData implements Serializable {
    public static final long serialVersionUID = 1L;
    /**
     * mqtt链接数据
     */
    private MqttConnectOptions connectOptions;

    public MqttConnectOptions getConnectOptions() {
        return connectOptions;
    }

    public MQTTServerConnData setConnectOptions(MqttConnectOptions connectOptions) {
        this.connectOptions = connectOptions;

        return this;
    }
}
