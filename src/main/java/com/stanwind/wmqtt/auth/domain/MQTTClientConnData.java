package com.stanwind.wmqtt.auth.domain;

import java.io.Serializable;

/**
 * MQTTConnData 客户端mqtt连接数据
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2020-11-20 14:40
 **/
public class MQTTClientConnData implements Serializable {
    public static final long serialVersionUID = 1L;

    private Long expire;
    private String username;
    private String password;

    public Long getExpire() {
        return expire;
    }

    public MQTTClientConnData setExpire(Long expire) {
        this.expire = expire;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public MQTTClientConnData setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public MQTTClientConnData setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        return "MQTTClientConnData{" +
                "expire=" + expire +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
